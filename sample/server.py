import eventlet
import socketio

from wsgi import app  # a Flask, Django, etc. application

sio = socketio.Server()
app = socketio.WSGIApp(sio,
                       static_files={
                           '/': {
                               'content_type': 'text/html',
                               'filename': 'index.html'
                           }
                       },
                       app)


@sio.event
def connect(sid, environ):
    print('connect ', sid)


@sio.event
def my_message(sid, data):
    print('message ', data)
    # send back crypto data to client
    sio.emit('client_message', {'BTC': '200000'})


@sio.event
def disconnect(sid):
    print('disconnect ', sid)


if __name__ == '__main__':
    eventlet.wsgi.server(eventlet.listen(('', 5000)), app)
