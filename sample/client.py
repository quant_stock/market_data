from time import sleep
import socketio

sio = socketio.Client()

sio.connect('http://localhost:5000')


@sio.on('client_message')
def client_message(data):
    print('Price Data ', data)


while True:
    sleep(2)
    # spam message to client
    sio.emit('my_message', {'message': 'Hello World!'})
