from asyncio import sleep
import pandas as pd
import numpy as np
import json
import hmac
import time
import websocket

socket = 'wss://ftx.com/ws/'

api_key = 'dodV4ltLnvp900mhelIIFZojvNRTqcLjGO_3bR8x'
secret_key = '_kJcYi4_NO4-ORzkn_aB5_tA_FRJT3cZ_XRliaPH'


def on_open(ws):
    print('connected')

    ts = int(time.time() * 1000)
    signa = hmac.new(secret_key.encode(), f'{ts}websocket_login'.encode(),
                     'sha256').hexdigest()
    auth = {'op': 'login', 'args': {'key': api_key, 'sign': signa, 'time': ts}}
    ws.send(json.dumps(auth))
    data = {'op': 'subscribe', 'channel': 'ticker', 'market': 'BTC-PERP'}
    ws.send(json.dumps(data))


def on_close(ws):
    # print('disconnected from server')
    print("Retry : %s" % time.ctime())
    time.sleep(10)
    connect_websocket()  # retry per 10 seconds


def connect_websocket():
    ws = websocket.WebSocketApp(socket,
                                on_open=on_open,
                                on_message=on_message,
                                keep_running=True)
    ws.run_forever()


def on_message(ws, message):
    print('got message')
    json_msg = json.loads(message)
    print(json_msg)


if __name__ == "__main__":
    try:
        connect_websocket()
    except Exception as err:
        print(err)
        print("connect failed")
