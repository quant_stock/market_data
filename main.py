import json
from symtable import Symbol
from flask import Flask, jsonify, request
from pycoingecko import CoinGeckoAPI
from flask_socketio import SocketIO

import requests

cg = CoinGeckoAPI()

app = Flask(__name__)
socketio = SocketIO(app)

bianance_test_net = "https://testnet.binance.vision/api"


@socketio.event
def my_message(sid, data):
    print('message ', data)


@app.route('/getdata/', methods=['GET'])
def getdata():
    coin_name = 'bitcoin'
    if request.args.get("coin_name"):
        coin_name = request.args.get("coin_name")

    data = cg.get_price(ids=coin_name, vs_currencies='usd')
    if len(data) == 0:
        data = cg.get_price(ids="bitcoin", vs_currencies='usd')
    socketio.emit('client_message', data)
    return jsonify(data)


def get_market():
    if request.args.get("market"):
        if request.args.get("market") == "FTX":
            return "FTX"
        elif (request.args.get("market") == "bitmex"):
            return "bitmex"
    return 'binance'


def call_get_market_data_api(market):

    if market == 'binance':
        symbol = 'BTCUSDT'
        if request.args.get("symbol"):
            symbol = request.args.get("symbol")
        bianance_test_net = "https://testnet.binance.vision/api/v3/trades"
        response = requests.get(bianance_test_net, params={
            'symbol': symbol
        }).json()

        return {
            'symbol': symbol,
            'price': response[0]['price'],
            'rise': response[0]['isBuyerMaker']
        }
    elif market == "FTX":
        symbol = 'BTC/USDT'
        if request.args.get("symbol"):
            symbol = request.args.get("symbol")
        ftx_test_net = "https://ftx.com/api/markets/%s/trades" % symbol
        response = requests.get(ftx_test_net).json()
        print(response)
        return {
            'symbol': symbol,
            'price': response['result'][0]['price'],
            'rise': response['result'][0]['side'] == "buy" if True else False
        }
    elif market == "bitmex":
        symbol = 'XBTUSDT'
        if request.args.get("symbol"):
            symbol = request.args.get("symbol")
        bitmex_test_net = "https://testnet.bitmex.com/api/v1/trade?symbol=XBT&count=1&reverse=true"
        response = requests.get(bitmex_test_net).json()
        return {
            'symbol': symbol,
            'price': response[0]['price'],
            'rise': response[0]['side'] == "Buy" if True else False
        }
    return ''


@app.route('/getmarketdata/', methods=['GET'])
def getmarketdata():
    market = get_market()
    data = call_get_market_data_api(market)
    return data


if __name__ == '__main__':
    socketio.run(app, debug=True)
